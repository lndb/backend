class CreateCreators < ActiveRecord::Migration[7.0]
  def change
    create_table :creators do |t|
      t.string :name, null: false, unique: true, index: true
      t.string :family_name, index: true
      t.string :given_name, index: true
      t.string :residence
      t.date :date_of_birth

      t.timestamps
    end
  end
end