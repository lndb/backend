class CreatePublishers < ActiveRecord::Migration[7.0]
  def change
    create_table :publishers do |t|
      t.string :name, null: false, default: ""
      t.references :publisher, null: true, foreign_key: true

      t.timestamps
    end

    add_index :publishers, :name, unique: true
  end
end