class CreateCreatorVolumes < ActiveRecord::Migration[7.0]
  def change
    create_table :creator_volumes do |t|
      t.references :creator, null: false, foreign_key: true
      t.references :volume, null: false, foreign_key: true
      t.references :role, null: false, foreign_key: true
      t.bigint :credited_as_id, null: true

      t.timestamps
    end

    add_foreign_key :creator_volumes, :aliases, column: :credited_as_id
  end
end