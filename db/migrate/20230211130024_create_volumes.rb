class CreateVolumes < ActiveRecord::Migration[7.0]
  def change
    create_table :volumes do |t|
      t.string :name, null: false
      t.references :series, null: false, foreign_key: true
      t.string :isbn10
      t.string :isbn13

      t.timestamps
    end
  end
end