class CreateCreatorAliases < ActiveRecord::Migration[7.0]
  def change
    create_table :creator_aliases do |t|
      t.references :creator, null: false, foreign_key: true, index: true
      t.references :alias, null: false, foreign_key: true, index: true

      t.timestamps
    end
  end
end