class CreateLabels < ActiveRecord::Migration[7.0]
  def change
    create_table :labels do |t|
      t.string :name, null: false, default: ""
      t.references :publisher, null: false, foreign_key: true

      t.timestamps
    end

    add_index :labels, :name, unique: true
  end
end