# frozen_string_literal: true
test_account = Account.find_or_create_by(email: "q@q.pl") do |test_account|
  test_account.password = "qweasdzxc"
  test_account.status = 2
end

test_account_profile = Profile.find_or_create_by(account_id: test_account.id)

fumiaki_maruto = Creator.find_or_create_by(family_name: "丸戸", given_name: "史明", name: "Fumiaki Maruto")
misaki_kurehito = Creator.find_or_create_by(family_name: "深崎", given_name: "暮人", name: "Misaki Kurehito")
cradle = Alias.find_or_create_by(name: "Cradle")
CreatorAlias.find_or_create_by(creator: misaki_kurehito, alias: cradle)

saekano = Series.find_or_create_by(name: "冴えない彼女の育てかた")
saekano_volume1 = Volume.find_or_create_by(name: saekano.name, series: saekano, isbn13: "978-4-04-071078-5")

illustrator = Role.find_or_create_by(name: "illustrator")
author = Role.find_or_create_by(name: "author")

CreatorVolume.find_or_create_by(creator: misaki_kurehito, volume: saekano_volume1, role: illustrator)
CreatorVolume.find_or_create_by(creator: fumiaki_maruto, volume: saekano_volume1, role: author)

reki_kawahara = Creator.find_or_create_by(family_name: "川原", given_name: "礫", name: "Reki Kawahara")
abec = Creator.find_or_create_by(name: "abec")
bunbun = Alias.find_or_create_by(name: "BUNBUN")
abec_alias = CreatorAlias.find_or_create_by(creator: abec, alias: bunbun)
sao = Series.find_or_create_by(name: "ソードアート・オンライン")
sao_volume1 = Volume.find_or_create_by(name: "ソードアート・オンライン1　アインクラッド", series: sao)

CreatorVolume.find_or_create_by(creator: reki_kawahara, volume: sao_volume1, role: author)
CreatorVolume.find_or_create_by(creator: abec, volume: sao_volume1, role: illustrator, credited_as: bunbun)