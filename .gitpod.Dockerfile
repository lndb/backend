FROM gitpod/workspace-base
ENV RTX_VER=1.3.0 RUBY_VERSION=3.2.0
RUN sudo install-packages autoconf bison patch build-essential rustc libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libgmp-dev libncurses5-dev libffi-dev libgdbm6 libgdbm-dev libdb-dev uuid-dev libpq-dev
RUN sudo wget https://github.com/jdxcode/rtx/releases/download/v${RTX_VER}/rtx-v${RTX_VER}-linux-x64 -O /usr/local/bin/rtx && \
    sudo chmod +x /usr/local/bin/rtx && \
    echo 'eval "$(rtx activate -s bash)"' >> ~/.bashrc && \
    rtx install ruby@${RUBY_VERSION} && \
    RTX_DEBUG=1 rtx exec ruby@${RUBY_VERSION} -- gem install bundler
COPY Gemfile Gemfile.lock ./
RUN RTX_DEBUG=1 rtx exec ruby@${RUBY_VERSION} -- bundle install --jobs=$(nproc)