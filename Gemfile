# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "~> 3.2"

gem "rails", "~> 7.0.4", ">= 7.0.4.2"
gem "sprockets-rails"
gem "pg", "~> 1.1"
gem "puma", "~> 5.0"
gem "importmap-rails"
gem "turbo-rails"
gem "stimulus-rails"
gem "jbuilder"
gem "redis", "~> 4.0"
gem "bootsnap", require: false
gem "bcrypt", "~> 3.1.7"
gem "dry-monads"
gem "dry-schema"
gem "dry-transaction"
gem "dry-types"
gem "dry-validation"
gem "rodauth-rails", "~> 1.7"
gem "jwt", "~> 2.7"

group :development, :test do
  gem "debug", platforms: %i[mri mingw x64_mingw]
  gem "factory_bot_rails"
  gem "factory_trace"
  gem "pg_query"
  gem "prosopite"
  gem "pry"
  gem "pry-byebug"
  gem "pry-rails"
  gem "relaxed-rubocop", require: false
  gem "rspec-rails", "~> 6.0.0"
  gem "rubocop", require: false
  gem "rubocop-performance", require: false
  gem "rubocop-rails", require: false
  gem "rubocop-rspec", require: false
  gem "rubocop-thread_safety", require: false
end

group :development do
  gem "web-console"
  gem "lefthook"
  gem "rack-mini-profiler"
  gem "solargraph"
  gem "solargraph-rails"
  gem "rails-erd"
end
