FactoryBot.define do
  factory :creator_alias do
    creator { nil }
    add_attribute(:alias) { nil }
  end
end