FactoryBot.define do
  factory :profile do
    account { nil }
    name { "MyString" }
    preferences { "" }
  end
end
