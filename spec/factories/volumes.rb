FactoryBot.define do
  factory :volume do
    name { "MyString" }
    series { nil }
    isbn10 { "MyString" }
    isbn13 { "MyString" }
  end
end
