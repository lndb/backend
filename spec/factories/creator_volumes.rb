FactoryBot.define do
  factory :creator_volume do
    creator { nil }
    volume { nil }
    role { nil }
    add_attribute(:alias) { nil }
  end
end