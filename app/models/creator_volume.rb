class CreatorVolume < ApplicationRecord
  belongs_to :creator
  belongs_to :volume
  belongs_to :role
  belongs_to :credited_as, class_name: "Alias", foreign_key: :credited_as_id, optional: true

  def credited_name
    credited_as.name if credited_as_id.present?

    creator.name
  end
end