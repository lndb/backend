class CreatorAlias < ApplicationRecord
  belongs_to :creator
  belongs_to :alias
end
