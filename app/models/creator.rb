class Creator < ApplicationRecord
  has_many :creator_aliases
  has_many :aliases, through: :creator_aliases

  has_many :creator_volumes
  has_many :volumes, through: :creator_volumes

  has_many :series, through: :volumes
end