class Profile < ApplicationRecord
  belongs_to :account

  store_accessor :preferences, :theme
end