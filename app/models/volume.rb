class Volume < ApplicationRecord
  belongs_to :series

  has_many :creator_volumes
  has_many :creators, through: :creator_volumes

  def creator_type(type)
    creator_volumes.joins(:creator, :role).preload(:credited_as).where(role: { name: type })
  end

  def authors
    creator_type("author").map(&:creator)
  end

  def author
    authors.first
  end

  def author_name
    creator_type("author").first.credited_name
  end

  def illustrators
    creator_type("illustrator").map(&:creator)
  end

  def illustrator
    illustrators.first
  end

  def illustrator_name
    creator_type("illustrator").first.credited_name
  end
end