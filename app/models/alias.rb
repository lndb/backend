class Alias < ApplicationRecord
  has_many :creator_aliases
  has_many :creators, through: :creator_aliases
end