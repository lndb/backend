class Publisher < ApplicationRecord
  belongs_to :publisher, optional: true
end